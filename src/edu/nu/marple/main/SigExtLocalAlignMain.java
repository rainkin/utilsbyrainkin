package edu.nu.marple.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.FilterWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.sound.midi.Sequence;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Commons;

import edu.nu.marple.Filter;
import edu.nu.marple.GlobalConfig;
import edu.nu.marple.Utils;
import edu.nu.marple.seqalign.NeedlemanWunsch;
import edu.nu.marple.seqalign.SmithWaterman;
import edu.nu.marple.seqalign.SequenceAlignment.Weights;

public class SigExtLocalAlignMain {

   public static void main(String[] args) {
	  List<String> phfs = Arrays.asList("Keylogger", "RemoteShell", "RemoteDesktop", "AudioRecord");
      phfs.forEach(t -> {
		try {
			extractSigsForPhf(t);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	});
   }
   
	public static void extractSigsForPhf(String phf) throws IOException {
		File dir = new File(GlobalConfig.getInstance().rootPath + "traces/" + phf + "/");
		File[] listsOfDir = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(".syscall_block500_uselessOld_filtered");
			}
		});

		BufferedWriter sigWriter = null;
		BufferedWriter sigDetailsWriter = null;
		sigWriter = new BufferedWriter(new FileWriter("generatedSignature/" + phf + ".txt"));
		sigDetailsWriter = new BufferedWriter(new FileWriter("generatedSignature/" + phf + "_details.txt"));

		for (int i = 0; i < listsOfDir.length - 1; i++) {
			File trace1File = listsOfDir[i];
			for (int j = i + 1; j < listsOfDir.length; j++) {
				File trace2File = listsOfDir[j];

				String trace1Phf = trace1File.getPath().split("\\\\")[1];
				String trace1Name = trace1File.getPath().split("\\\\")[2];
				String trace2Phf = trace2File.getPath().split("\\\\")[1];
				String trace2Name = trace2File.getPath().split("\\\\")[2];

				List<String> sequence1 = Utils.file2List(trace1File.getAbsolutePath());
				List<String> sequence2 = Utils.file2List(trace2File.getAbsolutePath());

				// Local Alignment
				// System.out.println("\nLocal Alignment");
				// System.out.println(trace1Name + " align " + trace2Name);
				SmithWaterman localAlign = new SmithWaterman(sequence1, sequence2, 4, -1, -1, Weights.NULL);

				// filter
				// List<String> uselessSyscallName = Arrays.asList(new String[]
				// {"NtYieldExecution", "NtAllocateVirtualMemory",
				// "NtUserWaitMessage", "NtUserMessageCall",
				// "NtRequestWaitReplyPort", "NtTestAlert", "NtDuplicateObject",
				// "NtUserPeekMessage", "NtQuerySystemTime",
				// "NtFreeVirtualMemory", "NtSetEvent", "NtWaitForSingleObject",
				// "NtDeviceIoControlFile", "NtCreateEvent",
				// "NtUserGetThreadState", "NtQueryPerformanceCounter",
				// "NtDelayExecution", "NtUserCallOneParam",
				// "NtUserCallTwoParam"});
				// List<String> uselessSyscallName = Arrays.asList(new String[]
				// {});
				List<String> commonSubSequence = localAlign.getMatchedAlignment();
				// commonSubSequence = commonSubSequence.stream().filter(e ->
				// !uselessSyscallName.contains(e)).collect(Collectors.toList());

				// remove duplicated
				// Filter filter = new Filter();
				// commonSubSequence = filter.filter(commonSubSequence, 1, 2);
				// commonSubSequence = new
				// Filter().mergeConsective(commonSubSequence);

				String signature = commonSubSequence.stream().collect(Collectors.joining(" -> "));
				sigWriter.write(signature + "\n");
				sigDetailsWriter.write(trace1File.getPath() + " ==align== " + trace2File.getPath() + "\n");
				sigDetailsWriter.write(signature + "\n\n");

			}

		}
		sigWriter.close();
		sigDetailsWriter.close();
	}

}
