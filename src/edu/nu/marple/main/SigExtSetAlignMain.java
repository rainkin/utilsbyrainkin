package edu.nu.marple.main;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


import edu.nu.marple.Filter;
import edu.nu.marple.GlobalConfig;
import edu.nu.marple.Utils;

public class SigExtSetAlignMain {

   public static void main(String[] args) throws IOException {
      File dir = new File(GlobalConfig.getInstance().rootPath + "traces/Keylogger/");
      File[] listsOfDir = dir.listFiles(new FilenameFilter() {
         public boolean accept(File dir, String name) {
            return name.toLowerCase().endsWith(".filtered");
         }
      });

      HashMap<String, Integer> syscallName2OccurNumber = new HashMap<String, Integer>();
      for (int i = 0; i < listsOfDir.length; i++) {
         File traceFile = listsOfDir[i];

//         System.out.println(traceFile.getName());

         List<String> sequence = Utils.file2List(traceFile.getAbsolutePath());
         for (String syscallName : sequence) {
            if (syscallName2OccurNumber.containsKey(syscallName)) {
               if (syscallName2OccurNumber.get(syscallName) < i) {
                  // for each trace file, the max occur number of one unique syscall name is 1, 
                  // even though there are several repeated syscall occuring in one file.
                  syscallName2OccurNumber.put(syscallName, syscallName2OccurNumber.get(syscallName) + 1);
               }
            } else {
               syscallName2OccurNumber.put(syscallName, 1);
            }
         }
      }
      
      // sorted by value
      syscallName2OccurNumber.entrySet().stream().sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue())).forEach(System.out::println);
      System.out.println();
      List<String> coreSyscallName = syscallName2OccurNumber.entrySet().stream().sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue())).filter(e -> e.getValue() > 6).map(e -> e.getKey()).collect(Collectors.toList());
//      List<String> uselessSyscallName = Arrays.asList(new String[] {"NtUserCallOneParam", "NtWaitForSingleObject", "NtDelayExecution", "NtDeviceIoControlFile", "NtAllocateVirtualMemory", "NtSetEvent", "NtQueryPerformanceCounter","NtUserPeekMessage"});
      List<String> uselessSyscallName = Arrays.asList(new String[] {"NtYieldExecution", "NtWaitForMultipleObjects", "NtOpenThreadToken",  "NtAllocateVirtualMemory", "NtUserWaitMessage", "NtUserMessageCall",  "NtRequestWaitReplyPort", "NtTestAlert", "NtDuplicateObject", "NtUserPeekMessage", "NtQuerySystemTime", "NtFreeVirtualMemory", "NtSetEvent", "NtWaitForSingleObject", "NtDeviceIoControlFile", "NtCreateEvent", "NtUserGetThreadState", "NtQueryPerformanceCounter", "NtDelayExecution", "NtUserCallOneParam", "NtUserCallTwoParam"});
      coreSyscallName.stream().forEach(System.out::println);;
      
      // find core sub sequence
      for (File file : listsOfDir){
         System.out.println("----- " + file.getName() + " ------");
         List<String> sequence = Utils.file2List(file.getAbsolutePath());         
         sequence = sequence.stream().filter(syscallName -> coreSyscallName.contains(syscallName) && !uselessSyscallName.contains(syscallName)).collect(Collectors.toList());
         // remove repeated consecutive syscall names
         sequence = new Filter().mergeConsective(sequence);
         sequence = new Filter().filter(sequence, 1, 2);
         sequence.stream().forEach(System.out::println);
         System.out.println("=============");
         
      }

   }

}
