package edu.nu.marple.main;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import edu.nu.marple.Filter;
import edu.nu.marple.GlobalConfig;
import edu.nu.marple.Utils;

public class FilterConsecutiveBlocksMain {

   public static void main(String[] args) throws IOException {
      File dir = new File(GlobalConfig.getInstance().rootPath + "traces/AudioRecord/");
      File[] files = dir.listFiles(new FilenameFilter() {
         public boolean accept(File dir, String name) {
            return name.toLowerCase().endsWith(".extracted");
         }
      });

      for (File json : files){
         System.out.println(json);
         
         List<String> sequence = Utils.file2List(json.getAbsolutePath());

         // remove duplicated sequences
         Filter filter = new Filter();
         sequence = filter.filterUselessSyscallsStringVersion(sequence, "importantList/useless_old.txt");
         sequence = filter.filterConsecutiveBlocks(sequence, 500);
         
         // write back to a new file
         File filteredTrace = new File(json.getAbsolutePath() + ".syscall_block500_uselessOld_filtered");
//         filteredTrace.createNewFile();
         FileWriter filteredWriter = new FileWriter(filteredTrace);
         filteredWriter.write(sequence.stream().collect(Collectors.joining("\n")));
         filteredWriter.flush();
         filteredWriter.close();
      }      
   }

}
