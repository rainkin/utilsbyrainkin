package edu.nu.marple.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.Utils;
import edu.nu.marple.seqalign.NeedlemanWunsch;
import edu.nu.marple.seqalign.SmithWaterman;
import edu.nu.marple.seqalign.SequenceAlignment.Weights;
import edu.nu.marple.sequence.LongestCommonSubsequence;
import javafx.scene.shape.Line;

public class Main {

   public static void main(String[] args) throws IOException {
      String trace1Path = GlobalConfig.getInstance().rootPath + "traces/Keylogger/DarkComet5.1@1_KeyLogger.json.extracted";
      String trace2Path = GlobalConfig.getInstance().rootPath + "traces/Keylogger/njrat0.7@1_Keylogger.json.extracted";
      List<String> sequence1 = Utils.file2List(trace1Path);
      List<String> sequence2 = Utils.file2List(trace2Path);
      
      String trace1Dir = trace1Path.split("/")[0];
      String trace1Phf = trace1Path.split("/")[1];
      String trace1Name = trace1Path.split("/")[2];
      
      String trace2Dir = trace2Path.split("/")[0];
      String trace2Phf = trace2Path.split("/")[1];
      String trace2Name = trace2Path.split("/")[2];
      
      long begin = 0L;
      long end = 0L;                

//      List<String> sequence1 = Arrays.asList("GCCCTAGCG".split(""));
//      List<String> sequence2 = Arrays.asList("GCGCAATG".split(""));
      
      
      // LCS
//      System.out.println("LCS");
//
//      LongestCommonSubsequence lcs = new LongestCommonSubsequence(sequence1, sequence2);
//      System.out.println(lcs.getLongestCommonSubsequence());
//      lcs.printScoreTable();
      
      // Local Alignment
      begin = System.currentTimeMillis();
      
      System.out.println("\nLocal Alignment");
      SmithWaterman localAlign = new SmithWaterman(sequence1, sequence2, 4,-1,-1, Weights.KEYLOGGER_NAME);
//      System.out.println(localAlign.getAlignment()[0]);
//      System.out.println(localAlign.getAlignment()[1]);
     
      localAlign.getUnifiedDiffAlignment();
      
      end = System.currentTimeMillis();      
      System.out.println("time: " + (end - begin)/1000 + "S"); 
            
      String htmlPath = "traces/" + trace1Phf + "/" + trace1Name + "_" + trace2Name + ".html";
      localAlign.diff2Html(htmlPath);
      
//      localAlign.printScoreTable();
      
      
//      // Global Alignment
//      System.out.println("\nGlobal Alignment");
//      NeedlemanWunsch globalAlign = new NeedlemanWunsch(sequence1, sequence2);
//      System.out.println(globalAlign.getAlignment()[0]);
//      System.out.println(globalAlign.getAlignment()[1]);
   }     
   

}
