package edu.nu.marple.seqalign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import edu.nu.marple.dp.Cell;
import edu.nu.marple.dp.MultiDirectionCell;

public abstract class SequenceAlignmentExtend<T> {
	protected List<T> sequence1;
	protected List<T> sequence2;
	protected MultiDirectionCell[][] scoreTable;
	protected int match;
	protected int gap;
	protected int mismatch;
	protected boolean tableIsFilledIn;
	protected boolean isInitialized;
	protected Map<String, Set<List<T>>> cache = new HashMap<String, Set<List<T>>>();


	public SequenceAlignmentExtend(List<T> sequence1, List<T> sequence2, int match, int mismatch, int gap) {
		this.sequence1 = sequence1;
		this.sequence2 = sequence2;
		this.match = match;
		this.mismatch = mismatch;
		this.gap = gap;
		scoreTable = new MultiDirectionCell[sequence2.size() + 1][sequence1.size() + 1];	
		this.tableIsFilledIn = false;
		this.isInitialized = false;
	}

	public SequenceAlignmentExtend(List<T> sequence1, List<T> sequence2) {
		this(sequence1, sequence2, 1, -1, -1);
	}

	/**
	 * initialize the score table
	 */
	protected void initialize() {
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[i].length; j++) {
				scoreTable[i][j] = new MultiDirectionCell(i, j);
			}
		}
		initializeScores();
		initializePointers();
		
		isInitialized = true;
	}

	protected void initializeScores() {
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[i].length; j++) {
				scoreTable[i][j].setScore(getInitialScore(i, j));
			}
		}
	}

	protected void initializePointers() {
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[i].length; j++) {
				scoreTable[i][j].setPrevCells(getInitialPointer(i, j));
			}
		}
	}

	protected abstract List<MultiDirectionCell> getInitialPointer(int row, int col);

	protected abstract int getInitialScore(int row, int col);

	
	/**
	 * fill in the score table using the initial table
	 */
	protected void fillIn() {
		for (int row = 1; row < scoreTable.length; row++) {
			for (int col = 1; col < scoreTable[row].length; col++) {
				MultiDirectionCell currentCell = scoreTable[row][col];
				MultiDirectionCell cellAbove = scoreTable[row - 1][col];
				MultiDirectionCell cellToLeft = scoreTable[row][col - 1];
				MultiDirectionCell cellAboveLeft = scoreTable[row - 1][col - 1];
				fillInCell(currentCell, cellAbove, cellToLeft, cellAboveLeft);
			}
		}
		
		tableIsFilledIn = true;
	}

	protected abstract void fillInCell(MultiDirectionCell currentCell, MultiDirectionCell cellAbove,
			MultiDirectionCell cellToLeft, MultiDirectionCell cellAboveLeft);
	
	
	/**
	 * find the matched alignment start from currentCell -> preCell
	 * @param currentCell
	 * @param preCell
	 * @return
	 */
	protected Set<List<T>> _getMatchedAlignment(MultiDirectionCell currentCell, MultiDirectionCell preCell) {
		Set<List<T>> currentMatchedSet = new HashSet<List<T>>();
		T matchedString = null;
		boolean isAbove = currentCell.getRow() - preCell.getRow() == 1;
		boolean isLeft = currentCell.getCol() - preCell.getCol() == 1;
		boolean isMatch = false;
		if (currentCell.getRow() == 0 || currentCell.getCol() == 0){
			isMatch = false;
		} else {
			isMatch = sequence2.get(currentCell.getRow() - 1).equals(sequence1.get(currentCell.getCol() - 1));
		}		

		if (isLeft && isAbove && isMatch) {
			matchedString = sequence2.get(currentCell.getRow() - 1);
		}

		if (preCell.getPrevCells() != null) {
			for (MultiDirectionCell prepreCell : preCell.getPrevCells()) {
				Set<List<T>> preMatchedSet = null;
				String cacheKey = preCell.getRow() + "#" + preCell.getCol() + "@" + prepreCell.getRow() + "#" +  prepreCell.getCol();
				if (cache.containsKey(cacheKey)){
					preMatchedSet = cache.get(cacheKey);
				} else {
					preMatchedSet = _getMatchedAlignment(preCell, prepreCell);
					cache.put(cacheKey, preMatchedSet);
				}
				
				Set<List<T>> finalMatchedSet = preMatchedSet.stream().map(e -> new ArrayList(e)).collect(Collectors.toSet());								
				if (matchedString != null) {
					if (!finalMatchedSet.isEmpty()) {
						for (List<T> matched : finalMatchedSet) {
							matched.add(matchedString);
						}
					} else {
						List<T> matched = new ArrayList<T>();
						matched.add(matchedString);
						finalMatchedSet.add(matched);
					}

				}
				currentMatchedSet.addAll(finalMatchedSet);
			}
		} else { // stop the recursive
			if (matchedString != null){
				List<T> matched = new ArrayList<T>();
				matched.add(matchedString);
				currentMatchedSet.add(matched);
			} 			
		}

		return currentMatchedSet;

	}

	/**
	 * Print the dynamic programming table of score
	 */
	public void printDPScoretable() {
		ensureTableIsFilledIn();
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[0].length; j++) {
				System.out.print(scoreTable[i][j].getScore() + "\t");
			}
			System.out.println("");
		}
	}

	/**
	 * Print the dynamic programming table of cells' direction size
	 */
	public void printDPDirectionTable() {
		ensureTableIsFilledIn();
		for (int i = 0; i < scoreTable.length; i++) {
			for (int j = 0; j < scoreTable[0].length; j++) {
				int directionSize = scoreTable[i][j].getPrevCells() == null ? 0
						: scoreTable[i][j].getPrevCells().size();
				System.out.print(directionSize + "\t");
			}
			System.out.println("");
		}
	}
	
	
	/**
	 * make sure score table and initialization have been done
	 */
	protected void ensureTableIsFilledIn() {
		if (!isInitialized) {
			initialize();
		}
		if (!tableIsFilledIn) {
			fillIn();
		}
	}
}
