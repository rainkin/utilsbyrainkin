package edu.nu.marple.seqalign;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.nu.marple.dp.Cell;
import edu.nu.marple.dp.MultiDirectionCell;
import edu.nu.marple.seqalign.SequenceAlignment.Weights;

public class SmithWatermanExtend<T> extends SequenceAlignmentExtend<T> {
	protected int topK;
	protected List<Set<MultiDirectionCell>> topKhighScoreCells;
	protected List<Integer> topKhighScores;
	protected int highScore;
	private List<Set<List<T>>> matchedAlignments;

	/**
	 * By default, the weight of mathc = 1, mismatch = -1, and gap = -1, topK =
	 * 1
	 * 
	 * @param sequence1
	 *            the sequences you want to compare
	 * @param sequence2
	 *            the sequences you want to compare
	 */
	public SmithWatermanExtend(List<T> sequence1, List<T> sequence2) {
		this(sequence1, sequence2, 1, -1, -1, 1);
	}

	/**
	 * The extend version of SmithWatern Algorithm, which support
	 * multi-direction and all highest score Cells' traceback
	 * 
	 * @param sequence1
	 *            the sequences you want to compare
	 * @param sequence2
	 *            the sequences you want to compare
	 * @param match
	 *            the weight of match
	 * @param mismatch
	 *            the weight of mismatch
	 * @param gap
	 *            the weight of space(gap)
	 * @param topK
	 *            the limit number of TopK highest score Cells
	 */
	public SmithWatermanExtend(List<T> sequence1, List<T> sequence2, int match, int mismatch, int gap, int topK) {
		super(sequence1, sequence2, match, mismatch, gap);
		this.topK = topK;
		highScore = 0;
		topKhighScoreCells = new ArrayList<Set<MultiDirectionCell>>();
		topKhighScores = new ArrayList<Integer>();
	}

	protected void fillInCell(MultiDirectionCell currentCell, MultiDirectionCell cellAbove,
			MultiDirectionCell cellToLeft, MultiDirectionCell cellAboveLeft) {
		int rowSpaceScore = cellAbove.getScore() + gap;
		int colSpaceScore = cellToLeft.getScore() + gap;
		int matchOrMismatchScore = cellAboveLeft.getScore();
		if (sequence2.get(currentCell.getRow() - 1).equals(sequence1.get(currentCell.getCol() - 1))) {
			matchOrMismatchScore += match;
		} else {
			matchOrMismatchScore += mismatch;
		}

		if (rowSpaceScore > colSpaceScore) {
			if (matchOrMismatchScore > rowSpaceScore) {
				if (matchOrMismatchScore > 0) {
					currentCell.setScore(matchOrMismatchScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore < rowSpaceScore) {
				if (rowSpaceScore > 0) {
					currentCell.setScore(rowSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAbove);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore == rowSpaceScore) {
				if (rowSpaceScore > 0) {
					currentCell.setScore(rowSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					preCells.add(cellAbove);
					currentCell.setPrevCells(preCells);

				}
			}
		} else if (rowSpaceScore < colSpaceScore) {
			if (matchOrMismatchScore > colSpaceScore) {
				if (matchOrMismatchScore > 0) {
					currentCell.setScore(matchOrMismatchScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore < colSpaceScore) {
				if (colSpaceScore > 0) {
					currentCell.setScore(colSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellToLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore == colSpaceScore) {
				if (colSpaceScore > 0) {
					currentCell.setScore(colSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					preCells.add(cellToLeft);
					currentCell.setPrevCells(preCells);
				}
			}
		} else if (rowSpaceScore == colSpaceScore) {
			if (matchOrMismatchScore > rowSpaceScore) {
				if (matchOrMismatchScore > 0) {
					currentCell.setScore(matchOrMismatchScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore < rowSpaceScore) {
				if (rowSpaceScore > 0) {
					currentCell.setScore(rowSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAbove);
					preCells.add(cellToLeft);
					currentCell.setPrevCells(preCells);
				}
			} else if (matchOrMismatchScore == rowSpaceScore) {
				if (rowSpaceScore > 0) {
					currentCell.setScore(rowSpaceScore);
					List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
					preCells.add(cellAboveLeft);
					preCells.add(cellToLeft);
					preCells.add(cellAbove);
					currentCell.setPrevCells(preCells);
				}
			}
		}

		addToTopkCells(currentCell);
	}

	private void addToTopkCells(MultiDirectionCell currentCell) {
		int currentScore = currentCell.getScore();
		boolean isInserted = false;

		// insert into existing cells
		for (Integer highScore : new ArrayList<Integer>(topKhighScores)) {
			if (currentScore > highScore) {
				isInserted = true;

				int insertIndex = topKhighScores.indexOf(highScore);
				// highest scores
				topKhighScores.add(insertIndex, currentScore);
				if (topKhighScores.size() > topK)
					topKhighScores.remove(topKhighScores.size() - 1);

				// highest score cells
				Set<MultiDirectionCell> insertCells = new HashSet<MultiDirectionCell>();
				insertCells.add(currentCell);
				topKhighScoreCells.add(insertIndex, insertCells);
				if (topKhighScoreCells.size() > topK)
					topKhighScoreCells.remove(topKhighScoreCells.size() - 1);

				break;
			} else if (currentScore == highScore) {
				isInserted = true;

				int insertIndex = topKhighScores.indexOf(currentScore);
				topKhighScoreCells.get(insertIndex).add(currentCell);

				break;
			}
		}

		// if the above insertion fails and topK has free rooms, append current
		// cell
		if (!isInserted && topKhighScores.size() < topK) {
			topKhighScores.add(currentScore);
			Set<MultiDirectionCell> insertCells = new HashSet<MultiDirectionCell>();
			insertCells.add(currentCell);
			topKhighScoreCells.add(insertCells);
		}
	}

	/**
	 * Get all matched alignment subsequences
	 * 
	 * @return a set of matched subsequences
	 */
	public List<Set<List<T>>> getMatchedAlignment() {
		ensureTableIsFilledIn();
		if (this.matchedAlignments != null)
			return this.matchedAlignments;

		this.matchedAlignments = new ArrayList<Set<List<T>>>();
		for (Set<MultiDirectionCell> cells : topKhighScoreCells) {
			Set<List<T>> matchedSet = new HashSet<List<T>>();
			for (MultiDirectionCell currentCell : cells) {
				if (currentCell.getPrevCells() != null) {
					for (MultiDirectionCell preCell : currentCell.getPrevCells()) {
						matchedSet.addAll(_getMatchedAlignment(currentCell, preCell));
					}
				}
			}
			this.matchedAlignments.add(matchedSet);
		}

		return this.matchedAlignments;
	}

	/**
	 * Get all matched alignment subsequences' scores
	 */
	public List<Integer> getMatchedAlignmentScores() {
		ensureTableIsFilledIn();
		if (!this.topKhighScores.isEmpty()) {
			return this.topKhighScores;
		} else {
			this.getMatchedAlignment();
			return this.topKhighScores;
		}

	}

	@Override
	protected List<MultiDirectionCell> getInitialPointer(int row, int col) {
		return null;
	}

	@Override
	protected int getInitialScore(int row, int col) {
		return 0;
	}

}
