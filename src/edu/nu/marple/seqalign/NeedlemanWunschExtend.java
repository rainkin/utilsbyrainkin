package edu.nu.marple.seqalign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.plaf.multi.MultiMenuItemUI;

import edu.nu.marple.dp.Cell;
import edu.nu.marple.dp.MultiDirectionCell;

public class NeedlemanWunschExtend<T> extends SequenceAlignmentExtend<T> {

	private Set<List<T>> matchedAlignments;
	private int higestScore;

	public NeedlemanWunschExtend(List<T> sequence1, List<T> sequence2) {
		this(sequence1, sequence2, 1, -1, -1);
	}

	public NeedlemanWunschExtend(List<T> sequence1, List<T> sequence2, int match, int mismatch, int gap) {
		super(sequence1, sequence2, match, mismatch, gap);
		higestScore = 0;
	}

	@Override
	protected List<MultiDirectionCell> getInitialPointer(int row, int col) {
		if (row == 0 && col != 0) {
			List<MultiDirectionCell> pointers = new ArrayList<MultiDirectionCell>();
			pointers.add(scoreTable[row][col - 1]);
			return pointers;
		} else if (col == 0 && row != 0) {
			List<MultiDirectionCell> pointers = new ArrayList<MultiDirectionCell>();
			pointers.add(scoreTable[row - 1][col]);
			return pointers;
		} else {
			return null;
		}
	}

	@Override
	protected int getInitialScore(int row, int col) {
		if (row == 0 && col != 0) {
			return col * gap;
		} else if (col == 0 && row != 0) {
			return row * gap;
		} else {
			return 0;
		}
	}

	@Override
	protected void fillInCell(MultiDirectionCell currentCell, MultiDirectionCell cellAbove,
			MultiDirectionCell cellToLeft, MultiDirectionCell cellAboveLeft) {
		int rowSpaceScore = cellAbove.getScore() + gap;
		int colSpaceScore = cellToLeft.getScore() + gap;
		int matchOrMismatchScore = cellAboveLeft.getScore();
		if (sequence2.get(currentCell.getRow() - 1).equals(sequence1.get(currentCell.getCol() - 1))) {
			matchOrMismatchScore += match;
		} else {
			matchOrMismatchScore += mismatch;
		}

		if (rowSpaceScore > colSpaceScore) {
			if (matchOrMismatchScore > rowSpaceScore) {
				currentCell.setScore(matchOrMismatchScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore < rowSpaceScore) {
				currentCell.setScore(rowSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAbove);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore == rowSpaceScore) {
				currentCell.setScore(rowSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				preCells.add(cellAbove);
				currentCell.setPrevCells(preCells);
			}
		} else if (rowSpaceScore < colSpaceScore) {
			if (matchOrMismatchScore > colSpaceScore) {
				currentCell.setScore(matchOrMismatchScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore < colSpaceScore) {
				currentCell.setScore(colSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellToLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore == colSpaceScore) {
				currentCell.setScore(colSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				preCells.add(cellToLeft);
				currentCell.setPrevCells(preCells);
			}
		} else if (rowSpaceScore == colSpaceScore) {
			if (matchOrMismatchScore > rowSpaceScore) {
				currentCell.setScore(matchOrMismatchScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore < rowSpaceScore) {
				currentCell.setScore(rowSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAbove);
				preCells.add(cellToLeft);
				currentCell.setPrevCells(preCells);
			} else if (matchOrMismatchScore == rowSpaceScore) {
				currentCell.setScore(rowSpaceScore);
				List<MultiDirectionCell> preCells = new ArrayList<MultiDirectionCell>();
				preCells.add(cellAboveLeft);
				preCells.add(cellToLeft);
				preCells.add(cellAbove);
				currentCell.setPrevCells(preCells);
			}
		}

	}

	/**
	 * Get the matched alignment subsequence
	 */
	public Set<List<T>> getMatchedAlignment() {
		ensureTableIsFilledIn();
		if (this.matchedAlignments != null) {
			return this.matchedAlignments;
		}

		MultiDirectionCell startCell = scoreTable[scoreTable.length - 1][scoreTable[0].length - 1];
		matchedAlignments = new HashSet<List<T>>();
		for (MultiDirectionCell preCell : startCell.getPrevCells()) {
			matchedAlignments.addAll(_getMatchedAlignment(startCell, preCell));
		}
		return matchedAlignments;
	}

	

	/**
	 * Get the higest score of global alignment
	 */
	public int getMatchedAlignmentScores() {
		ensureTableIsFilledIn();
		if (this.higestScore != 0) {
			return this.higestScore;
		} else {
			this.higestScore = scoreTable[scoreTable.length - 1][scoreTable[0].length - 1].getScore();
			return this.higestScore;
		}
	}

}
