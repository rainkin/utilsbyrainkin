package edu.nu.marple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * Longest Consecutive Repeated Substring
 */
public class LRS {

   protected HashMap<List<String>, Integer> suffix2startIndex = new HashMap<List<String>, Integer>();
   protected List<String> string;
   protected Set<Integer> lrsStartIndexSet = new HashSet<Integer>();
   protected int lrsLength = 0;
   protected List<String> lrsString = new ArrayList<String>();
   protected int searchDepth;
   protected int lrsMinLength = 2;
   
   public LRS(List<String> string, int searchDepth, int lrsMinLength){      
      this.string = string;
      this.searchDepth = searchDepth;
      this.lrsMinLength = lrsMinLength;
      lrs();
   }
   
   public LRS(List<String> string){
      this(string, 1, 1);
   }
   
   // return the longest common prefix of s and t
   public Substring lcp(List<String> s, List<String> t) {
      int maxLrsLength = Math.abs(s.size() - t.size());
      int mismatchedIndex = -1;
      int n = Math.min(s.size(), t.size());
      for (int i = 0; i < n; i++) {
         if (!s.get(i).equals(t.get(i))) {
            mismatchedIndex = i;
            break;
         } else {
            boolean isOverlap =  maxLrsLength < i + 1;
            if (isOverlap){
               return new Substring();
            }
         }
      }
      int repeatedLength = n;
      if (mismatchedIndex != -1)
         repeatedLength = mismatchedIndex;

      boolean isOverlap = maxLrsLength < repeatedLength;
      if (isOverlap) {
         return new Substring();
      } else {
         int subOneStartIndex = suffix2startIndex.get(s);
         int subTwoStartIndex = suffix2startIndex.get(t);
         int length = repeatedLength;
         return new Substring(subOneStartIndex, subTwoStartIndex, length, s.subList(0, repeatedLength));         
      }
   }

   // return the longest repeated string in s
   public void lrs() {      
      
      // form the N suffixes
      int n = string.size();
      List<List<String>> suffixes = new ArrayList<List<String>>();
      for (int i = 0; i < n; i++) {
         suffixes.add(i, string.subList(i, n));
         suffix2startIndex.put(string.subList(i, n), i);
      }

      // sort them
      Collections.sort(suffixes, new ListStringComparator());
      
      // find longest repeated substring by comparing adjacent sorted suffixes
      Substring lrs = new Substring();
      for (int i = 0; i < n - 1; i++) {
         for (int depth = 1; depth <= searchDepth && i + depth < suffixes.size(); depth++){
            
            Substring substr = lcp(suffixes.get(i), suffixes.get(i + depth));         
            if (substr.length > lrs.length){
               if (substr.length >= this.lrsMinLength){
                  lrs = substr;
                  lrsStartIndexSet.clear();
                  lrsStartIndexSet.add(lrs.subOneStartIndex);
                  lrsStartIndexSet.add(lrs.subTwoStartIndex);
                  lrsLength = lrs.length;            
                  lrsString = lrs.string;
//                  System.out.println(lrsStartIndexSet + " " + lrsLength);
//                  System.out.println("====");
               }               
            } else if (substr.length == lrs.length && substr.length > 0) {               
               if (new ListStringComparator().compare(substr.string, lrs.string) == 0){
                  boolean isOneOverlap = false;
                  boolean isTwoOverlap = false;
                  for (Integer index : new HashSet<Integer>(lrsStartIndexSet)){
                     if (Math.abs(substr.subTwoStartIndex - index) < substr.length)
                        isTwoOverlap = true;
                     if (Math.abs(substr.subOneStartIndex - index) >= substr.length)
                        isOneOverlap = true;
                  }     
                  if (!isOneOverlap)
                     lrsStartIndexSet.add(substr.subOneStartIndex);
                  if (!isTwoOverlap)
                     lrsStartIndexSet.add(substr.subTwoStartIndex);
               }
            }
         }
         
      }
//      System.out.println(System.currentTimeMillis() - start);

   }

   public Set<Integer> getLrsStartIndexSet(){
      return this.lrsStartIndexSet;
   }
   
   public int getLrsLength(){
      return this.lrsLength;
   }
   
   public String getLrsString(){
      return this.lrsString.toString();      
   }
   public static void main(String[] args) {
      String s = "a a a ";
//      String s= "a a a a a a a a a a a ";
      List<String> stringList = Arrays.asList(s.split("(\n|\r|\t| )+"));
      LRS lrs = new LRS(stringList, 1, 1);
      System.out.println(lrs.getLrsString());
      System.out.println(lrs.getLrsStartIndexSet());
   }
}

class Substring{
   public List<String> string;
   public int subOneStartIndex = 0;   
   public int subTwoStartIndex = 0;
   public int length = 0;
   
   public Substring(){
      this(0, 0, 0, new ArrayList<String>());
   }
   
   public Substring(int subOneStartIndex, int subTwoStartIndex, int length, List<String> string){
      this.subOneStartIndex = subOneStartIndex;
      this.subTwoStartIndex = subTwoStartIndex;
      this.length = length;
      this.string = string;
   }
}

class ListStringComparator implements Comparator<List<String>> {
   @Override
   public int compare(List<String> former, List<String> latter) {      
      int min = Math.min(former.size(), latter.size());
      for (int i = 0; i < min; i++){
         int compareResult = former.get(i).compareTo(latter.get(i));
         if (compareResult != 0){
            return compareResult;
         }
      }
      
      if (former.size() > min)
         return 1;
      else if (latter.size() > min)
         return -1;
      else 
         return 0;
   }
}