package edu.nu.marple;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Utils {

   public static List<String> file2List(String filepath) throws IOException{
      File file = new File(filepath);
      BufferedReader reader = new BufferedReader(new FileReader(file));
      ArrayList<String> sequences = new ArrayList<>();
      
      String line = null;
      while((line = reader.readLine()) != null)
      {
         sequences.add(line);
      }
      
      return sequences;
   }
   
   
}
