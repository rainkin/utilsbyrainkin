package edu.nu.marple;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import com.ibm.marple.SyscallRecordGeneration;

public class Filter<T> {
   

   public List<String> filter(List<String> sequences){
      return filter(sequences, 1, 1);
   }
   
   /**
    * filter useless syscalls (SyscallRecordGeneration Version)
    */
   public List<SyscallRecordGeneration> filterUselessSyscalls(List<SyscallRecordGeneration> sequences, String uselessSyscallsFilePath) throws FileNotFoundException{
	   // load useless syscalls
	   Scanner scanner =  new Scanner(new File(uselessSyscallsFilePath));
	   HashSet<String> uselessSyscalls = new HashSet<String>();
	   while (scanner.hasNext()){
		   uselessSyscalls.add(scanner.next());
	   }
	   scanner.close();
	   
	   // filter
	   return sequences.stream()
	    		.filter(e -> !uselessSyscalls.contains(e.name))
	    		.collect(Collectors.toList());
	   
   }
   
   /*
    * filter useless syscalls (String Version)
    */
   public List<String> filterUselessSyscallsStringVersion(List<String> sequences, String uselessSyscallsFilePath) throws FileNotFoundException{
	   // load useless syscalls
	   Scanner scanner =  new Scanner(new File(uselessSyscallsFilePath));
	   HashSet<String> uselessSyscalls = new HashSet<String>();
	   while (scanner.hasNext()){
		   uselessSyscalls.add(scanner.next());
	   }
	   scanner.close();
	   
	   // filter
	   return sequences.stream()
	    		.filter(e -> !uselessSyscalls.contains(e))
	    		.collect(Collectors.toList());
   }
   
   /**
    * remove consecutive and repeated blocks. e.g. a b a b x z -> a b x z (repeated and consecutive)  e.g. a b x a b -> a b x a b (repeated but not consecutive)
    * @param sequences
    * @param maxLengthOfConsecutivBlock the max length of the repeated and consecutive substring, which will be removed 
    * @return filtered sequences
    */
   public List<T> filterConsecutiveBlocks(List<T> sequences, int maxLengthOfConsecutiveBlock){
      if (maxLengthOfConsecutiveBlock <= 0)
         return sequences;
      
      List<T> mergedSequences = new ArrayList<T>(sequences); 
      for (int i = 0; i < maxLengthOfConsecutiveBlock; i++){
         mergedSequences = mergeNLengthConsective(mergedSequences, i);
      }
      return mergedSequences; 
   }
   
   /**
    * remove repeated blocks by LRS, whiose length must >= lrsMinLength. e.g. a b x a b -> a b x
    * @param sequences
    * @param searchDepth how many suffixs should been compare
    * @param lrsMinLength the minimal length of the longest repeated substring
    * @return filtered sequence s
    */
   public List<String> filter(List<String> sequences, int searchDepth, int lrsMinLength){
      List<String> mergedSequences = new ArrayList<String>(sequences); 
      // merge consecutive duplicated sequences      
//      mergedSequences = mergeNLengthConsective(mergedSequences, 1);
//      mergedSequences = mergeNLengthConsective(mergedSequences, 2);
      mergedSequences = mergeConsective(mergedSequences);
      
      // find longest repeated substring
      List<Integer> lrsStartIndexList = null;
      do{
//         System.out.println("----------------LRS----------------");
         LRS lrs = new LRS(mergedSequences, searchDepth, lrsMinLength);
         int lrsLength = lrs.getLrsLength();
         Set<Integer> lrsStartIndexSet = lrs.getLrsStartIndexSet();
         lrsStartIndexList = new ArrayList<Integer>(lrsStartIndexSet);
         Collections.sort(lrsStartIndexList);         
         for (int i = lrsStartIndexList.size() - 1; i > 0; i--){            
            for (int j = lrsStartIndexList.get(i) + lrsLength - 1 ; j >= lrsStartIndexList.get(i); j--){               
//               System.out.println(mergedSequences.get(j));
               mergedSequences.remove(j); 
            }
         }         
        
      } while(lrsStartIndexList.size() != 0);
      
      return mergedSequences;
   }
   
   public List<T> mergeNLengthConsective(List<T> sequences, int n){
      if (sequences.isEmpty() || n > sequences.size()/2 + 1 || n <= 0)
         return sequences;
      
      List<T> finalSequences = new ArrayList<T>(sequences);
      for (int i = 0; i < n; i++){         
         List<T> mergedSequences = new ArrayList<T>();
         mergedSequences.addAll(finalSequences.subList(0, i));
         List<T> mergedTmp = _mergeNLengthConsective(finalSequences.subList(i, finalSequences.size()), n);         
         mergedSequences.addAll(mergedTmp);
         finalSequences = mergedSequences;
      }
      return finalSequences;
   }
   
   private List<T> _mergeNLengthConsective(List<T> sequences, int n){      
      if (sequences.isEmpty() || n > sequences.size()/2 + 1)
         return sequences;
      
      List<T> mergedSequences = new ArrayList<T>();
//      System.out.println("=====merge=====" + " " + n);
      int i = 0;
      for (; i < sequences.size() + 1 - 2*n; i = i + n){
         if (!sequences.subList(i, i+n).equals(sequences.subList(i+n, i+2*n))){            
            mergedSequences.addAll(sequences.subList(i, i+n));
         }
//         else {
//            System.out.println(sequences.subList(i, i+n));
//         }
      }
      mergedSequences.addAll(sequences.subList(i, sequences.size()));
//      System.out.println("+++++size+++++ " + mergedSequences.size());
      return mergedSequences;
   }
   
   public List<String> mergeConsective(List<String> sequences){
      if (sequences.isEmpty())
         return sequences;
         
      List<String> mergedSequences = new ArrayList<String>(); 
      for (int i = 0; i < sequences.size() - 1; i++){
         if (!sequences.get(i).equals(sequences.get(i+1))){
            mergedSequences.add(sequences.get(i));
         }
      }      
      mergedSequences.add(sequences.get(sequences.size()-1));
      return mergedSequences;
   }
   
   
   public static void main(String[] args) throws IOException {
       String trace1Path = GlobalConfig.getInstance().rootPath + "traces/Keylogger/pandora2.2@1_Keylogger.json.extracted";
       List<String> sequence = Utils.file2List(trace1Path);
       // remove duplicated sequences
       Filter filter = new Filter();
       sequence = filter.filter(sequence, 1, 2);
       System.out.println("==================results " + sequence.size());
       System.out.println(sequence.stream().collect(Collectors.joining("\n")));
   }
}
