import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.ibm.marple.SyscallRecordGeneration;

import edu.nu.marple.Filter;
import edu.nu.marple.Utils;

public class FilterTest {

	public static void main(String[] args) throws IOException {
//		String s1 = "a b a b x y z a b";
//		List<String> sequence = Arrays.asList(s1.split("(\n|\r|\t| )+"));
      List<SyscallRecordGeneration> sequence = new ArrayList<SyscallRecordGeneration>();
      SyscallRecordGeneration a1 = new SyscallRecordGeneration("A", "a1");
      SyscallRecordGeneration a2 = new SyscallRecordGeneration("A", "a2");
      SyscallRecordGeneration b = new SyscallRecordGeneration("B", "b");
      SyscallRecordGeneration bb = new SyscallRecordGeneration("B", "b");
      SyscallRecordGeneration c = new SyscallRecordGeneration("C", "c");
      SyscallRecordGeneration d = new SyscallRecordGeneration("D", "d");
      SyscallRecordGeneration e = new SyscallRecordGeneration("NtWaitForSingleObject", "nt");
      SyscallRecordGeneration g = new SyscallRecordGeneration("NtWaitForSingleObject", "nt");
      sequence.add(a1);
      sequence.add(a1);
      sequence.add(a2);
      sequence.add(b);
      sequence.add(b);
      sequence.add(b);
      sequence.add(c);
      sequence.add(d);
		
		/* remove duplicated sequences */
      	List<SyscallRecordGeneration> mergedSequence = null;
		
      	// remove repeated blocks by LRS
		Filter filter = new Filter();
//		mergedSequence = filter.filter(sequence, 1, 2);
//		System.out.println("================== remoev repeated blocks " + mergedSequence.size());
//		System.out.println(mergedSequence.stream().collect(Collectors.joining("\n")));

		// remvoe repeated and consecutive blocks 
		mergedSequence = filter.filterConsecutiveBlocks(sequence, 30);		
		System.out.println("================== remoev repeated and consecutive blocks " + mergedSequence.size());
		mergedSequence.forEach(System.out::println);
		
		// filter useless syscalls
		mergedSequence = filter.filterUselessSyscalls(mergedSequence, "coresyscall/useless");
		System.out.println("================== filter useless syscalls " + mergedSequence.size());
		mergedSequence.forEach(System.out::println);

	}

}
