import java.util.Arrays;
import java.util.List;

import edu.nu.marple.seqalign.SmithWaterman;
import edu.nu.marple.seqalign.SequenceAlignment.Weights;

public class SmithWatermanTest {

	public static void main(String[] args) {
      List<String> sequence1 = Arrays.asList("GCCCTAGCG".split(""));
      List<String> sequence2 = Arrays.asList("GCGCAATG".split(""));
      
      System.out.println("\nLocal Alignment");
      SmithWaterman localAlign = new SmithWaterman(sequence1, sequence2, 1,-1,-1, Weights.NULL);
      System.out.println(localAlign.getMatchedAlignment());
	}

}
