import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.nu.marple.GlobalConfig;
import edu.nu.marple.Utils;
import edu.nu.marple.seqalign.SmithWatermanExtend;

public class SmithWatermanExtendTest {

   public static void main(String[] args) throws IOException {
//      String trace1Path = GlobalConfig.getInstance().rootPath + "traces/Keylogger/DarkComet5.1@1_KeyLogger.json.extracted.filtered";
//      String trace2Path = GlobalConfig.getInstance().rootPath + "traces/Keylogger/njrat0.7@1_Keylogger.json.extracted.filtered";
//      List<String> sequence1 = Utils.file2List(trace1Path);
//      List<String> sequence2 = Utils.file2List(trace2Path);
//      SmithWatermanExtend swextend = new SmithWatermanExtend(sequence1, sequence2, 4, -1, -1, 1);
////      swextend.printDPScoretable();
////      System.out.println("direction===============================");
////      swextend.printDPDirectionTable();
//      System.out.println("======results=====");
//      swextend.getMatchedAlignment().forEach(System.out::println);
//      System.out.println("======Ground True results=====");
//      System.out.println("[[NtDelayExecution, NtDelayExecution, NtUserGetKeyboardState, NtUserMapVirtualKeyEx, NtUserToUnicodeEx, NtUserGetForegroundWindow, NtUserQueryWindow, NtDelayExecution, NtUserGetKeyState, NtDelayExecution, NtUserGetKeyState, NtUserGetKeyboardState, NtUserMapVirtualKeyEx, NtUserGetForegroundWindow, NtUserQueryWindow, NtUserToUnicodeEx, NtUserGetForegroundWindow, NtUserQueryWindow, NtDelayExecution, NtUserGetKeyState, NtUserGetKeyboardState, NtUserMapVirtualKeyEx, NtUserGetForegroundWindow, NtUserQueryWindow, NtUserGetForegroundWindow, NtUserQueryWindow, NtDelayExecution]]");
//      
//      

	   
	   
//	   String s1 = "G C C C T A G C G A A";
//      List<String> sequence1 = Arrays.asList(s1.split("(\n|\r|\t| )+"));
//      String s2 = "G C G G A A T G";
//      List<String> sequence2 = Arrays.asList(s2.split("(\n|\r|\t| )+"));
      
      List<UnionSyscallArgs> sequence1 = new ArrayList<UnionSyscallArgs>();
      UnionSyscallArgs a1 = new UnionSyscallArgs("A", "a1");
      UnionSyscallArgs a2 = new UnionSyscallArgs("A", "a2");
      UnionSyscallArgs b = new UnionSyscallArgs("B", "b");
      UnionSyscallArgs c = new UnionSyscallArgs("C", "c");
      UnionSyscallArgs d = new UnionSyscallArgs("D", "d");            
      sequence1.add(a1);
      sequence1.add(b);
      sequence1.add(c);
      sequence1.add(d);
            
      List<UnionSyscallArgs> sequence2 = new ArrayList<>();
      sequence2.add(a2);
      sequence2.add(b);
      sequence2.add(c);
      sequence2.add(d);
      sequence2.add(d);

      SmithWatermanExtend swextend = new SmithWatermanExtend(sequence1,
            sequence2, 1, -1, -2, 4);
      System.out.println("====result====");
      swextend.getMatchedAlignment().forEach(System.out::println);
      System.out.println("====ground truth result====");
      System.out.println("[[G, C, G, A, A], [G, A, A], [G, C, G, A], [G, C, G]]\n" + 
    		  			 "[[G, C, G, A], [G, C, G], [G, A], [G, C]]\n" + 
    		  			 "[[A], [C], [T], [G], [G, C, G, A, A], [G, A, A], [G, C, G], [G, C]]\n" + 
    		  			 "[]\n");
      System.out.println("\n==score==");
      swextend.getMatchedAlignmentScores().forEach(System.out::println);
      System.out.println("\n==score table==");
      swextend.printDPScoretable();

   }
   


}

class UnionSyscallArgs{
   public String syscall;
   public String args;
   public UnionSyscallArgs(String syscall, String args){
      this.syscall = syscall;
      this.args = args;
   }
   
   public boolean equals(UnionSyscallArgs obj) {
      return this.syscall.equals(obj.syscall) && this.args.equals(obj.args);
   }

   @Override
   public String toString() {
      return syscall + " " + args ;
   }
   
   
   
}
